import { createMuiTheme } from '@material-ui/core'

const classic = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#d60000',
        },
        secondary: {
            main: '#070707'
        },
        topBar: {
            main: '#1c1c1c',
            textOverHome: '#fff',
            text: '#d60000',
        },
        footer: {
            main: '#1c1c1c',
            text: '#d60000',
        },
        text: {
            homeTitle: '#fff',
            homeSubtitle: '#fff',
            sectionTitle: '#fff',
            h3: '#fff',
            main: '#a0a0a0',
        },
        shadow: {
            main: '#000',
            light: '#242424',
        },
        background: {
            primary: '#101010',
            secondary: '#131313',
            tertiary: '#1c1c1c'
        },
    },

    image: {
        home: 'backgroundRedHacker.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Roboto',
            'Merriweather',
            'Lato'
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
classic.typography.h1 = {
    fontFamily: 'Merriweather',
    [classic.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [classic.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [classic.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [classic.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
classic.typography.subtitle1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    [classic.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [classic.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [classic.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
classic.typography.h2 = {
    fontFamily: 'Merriweather',
    fontWeight: 'normal',
    fontSize: '35pt',
    [classic.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [classic.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
classic.typography.h3 = {
    fontFamily: 'Roboto',
    fontWeight:  'bold',
    color: classic.palette.text.h3,
    [classic.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [classic.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [classic.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


classic.typography.body1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    color: classic.palette.text.main,
    [classic.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [classic.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [classic.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default classic