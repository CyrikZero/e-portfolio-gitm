import { createMuiTheme } from '@material-ui/core'

const savannah = createMuiTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#3C4F2A',
        },
        secondary: {
            main: '#E9FFCC'
        },
        topBar: {
            main: '#89B560',
            textOverHome: '#fff',
            text: '#000',
        },
        footer: {
            main: '#3C4F2A',
            text: '#fafafa',
        },
        text: {
            homeTitle: '#fff',
            homeSubtitle: '#fff',
            sectionTitle: '#000',
            h3: '#000',
            main: '#191919',
        },
        shadow: {
            main: '#0000004d',
            light: '#00000020',
        },
        background: {
            primary: '#A6DB74',
            secondary: '#E9FFCC',
            tertiary: '#769C52'
        },
    },

    image: {
        home: 'backgroundSavannah.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Lato'
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
savannah.typography.h1 = {
    fontFamily: 'Lato',
    [savannah.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [savannah.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [savannah.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [savannah.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
savannah.typography.subtitle1 = {
    fontFamily: 'Lato',
    fontWeight: 'normal',
    [savannah.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [savannah.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [savannah.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
savannah.typography.h2 = {
    fontFamily: 'Lato',
    fontWeight: 'normal',
    fontSize: '35pt',
    [savannah.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [savannah.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
savannah.typography.h3 = {
    fontFamily: 'Lato',
    fontWeight:  'bold',
    color: savannah.palette.text.h3,
    [savannah.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [savannah.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [savannah.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


savannah.typography.body1 = {
    fontFamily: 'Lato',
    fontWeight: 'normal',
    color: savannah.palette.text.main,
    [savannah.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [savannah.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [savannah.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default savannah