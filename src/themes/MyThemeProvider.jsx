import React from 'react'
import { Theme } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core'
import classic from './classic'
import blueShades from "./blueShades";
import darkBlue from './darkBlue'
import greenIT from './greenIT'
import redHacker from './redHacker'
import savannah from './savannah'
import unicorn from './unicorn'
import wood from "./wood"

const themeMap: { [key: string]: Theme } = {
    classic,
    blueShades,
    darkBlue,
    greenIT,
    redHacker,
    savannah,
    unicorn,
    wood,
}

export default function MyThemeProvider(props) {
    const { name, children } = props
    const theme = themeMap[name]

    return (
        <MuiThemeProvider theme={theme}>
            {children}
        </MuiThemeProvider>
    )
}
