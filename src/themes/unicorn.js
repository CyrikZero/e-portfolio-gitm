import { createMuiTheme } from '@material-ui/core'

const unicorn = createMuiTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#ffc0cb',
        },
        secondary: {
            main: '#fff'
        },
        topBar: {
            main: '#fff',
            textOverHome: '#000',
            text: '#000',
        },
        footer: {
            main: '#ffc0cb',
            text: '#000',
        },
        text: {
            homeTitle: '#000',
            homeSubtitle: '#000',
            sectionTitle: '#000',
            h3: '#000',
            main: '#000',
        },
        shadow: {
            main: '#0000004d',
            light: '#00000026',
        },
        background: {
            primary: '#FCFCFC',
            secondary: '#F6F6F6',
            tertiary: '#fff'
        },
    },

    image: {
        home: 'backgroundUnicorn.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Roboto',
            'Merriweather',
            'Lato'
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
unicorn.typography.h1 = {
    fontFamily: 'Merriweather',
    [unicorn.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [unicorn.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [unicorn.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [unicorn.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
unicorn.typography.subtitle1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    [unicorn.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [unicorn.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [unicorn.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
unicorn.typography.h2 = {
    fontFamily: 'Merriweather',
    fontWeight: 'normal',
    [unicorn.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [unicorn.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
unicorn.typography.h3 = {
    fontFamily: 'Roboto',
    fontWeight:  'bold',
    color: unicorn.palette.text.h3,
    [unicorn.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [unicorn.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [unicorn.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


unicorn.typography.body1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    color: unicorn.palette.text.main,
    [unicorn.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [unicorn.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [unicorn.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

unicorn.typography.body2 = {
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    color: unicorn.palette.primary.main,
    [unicorn.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [unicorn.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [unicorn.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default unicorn
