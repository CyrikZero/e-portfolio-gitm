import { createMuiTheme } from '@material-ui/core'

const wood = createMuiTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#404a58',
        },
        secondary: {
            main: '#fff'
        },
        topBar: {
            main: '#fff',
            textOverHome: '#fff',
            text: '#323c4b',
        },
        footer: {
            main: '#b9885d',
            text: '#364655',
        },
        text: {
            homeTitle: '#fff',
            homeSubtitle: '#fff',
            sectionTitle: '#323c4b',
            h3: '#3d4250',
            main: '#364655',
        },
        shadow: {
            main: '#0000004d',
            light: '#00000020',
        },
        background: {
            primary: '#d19e6f',
            secondary: '#f9f9f9',
            tertiary: '#f5f5f5'
        },
    },

    image: {
        home: 'backgroundWood.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Roboto',
            'Merriweather',
            'Lato'
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
wood.typography.h1 = {
    fontFamily: 'Merriweather',
    [wood.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [wood.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [wood.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [wood.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
wood.typography.subtitle1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    [wood.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [wood.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [wood.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
wood.typography.h2 = {
    fontFamily: 'Merriweather',
    fontWeight: 'normal',
    fontSize: '35pt',
    [wood.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [wood.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
wood.typography.h3 = {
    fontFamily: 'Roboto',
    fontWeight:  'bold',
    color: wood.palette.text.h3,
    [wood.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [wood.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [wood.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


wood.typography.body1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    color: wood.palette.text.main,
    [wood.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [wood.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [wood.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default wood