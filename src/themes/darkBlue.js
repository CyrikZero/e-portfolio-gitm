import { createMuiTheme } from '@material-ui/core'

const darkBlue = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#1C2833',
        },
        secondary: {
            main: '#D0D3D4'
        },
        topBar: {
            main: '#2C3E50',
            textOverHome: '#fff',
            text: '#D0D3D4',
        },
        footer: {
            main: '#2C3E50',
            text: '#D0D3D4',
        },
        text: {
            homeTitle: '#fff',
            homeSubtitle: '#D0D3D4',
            sectionTitle: '#D0D3D4',
            h3: '#D0D3D4',
            main: '#D0D3D4',

        },
        shadow: {
            main: '#0000004d',
            light: '#00000020',
        },
        background: {
            primary: '#273746',
            secondary: '#212F3D',
            tertiary: '#2E4053'
        },
    },

    image: {
        home: 'backgroundDarkBlue2.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Lato',
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
darkBlue.typography.h1 = {
    fontFamily: 'Lato',
    [darkBlue.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [darkBlue.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [darkBlue.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [darkBlue.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
darkBlue.typography.subtitle1 = {
    fontFamily: 'Lato',
    fontWeight: 'normal',
    [darkBlue.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [darkBlue.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [darkBlue.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
darkBlue.typography.h2 = {
    fontFamily: 'Lato',
    fontWeight: 'normal',
    [darkBlue.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [darkBlue.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
darkBlue.typography.h3 = {
    fontFamily: 'Lato',
    fontWeight:  'bold',
    color: darkBlue.palette.text.h3,
    [darkBlue.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [darkBlue.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [darkBlue.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


darkBlue.typography.body1 = {
    fontFamily: 'Lato',
    fontWeight: 'normal',
    color: darkBlue.palette.text.main,
    [darkBlue.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [darkBlue.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [darkBlue.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default darkBlue
