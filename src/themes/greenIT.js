import { createMuiTheme } from '@material-ui/core'

const greenIT = createMuiTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#81a725',
        },
        secondary: {
            main: '#fffefd'
        },
        topBar: {
            main: '#303538',
            textOverHome: '#fff',
            text: '#81a725',
        },
        footer: {
            main: '#303538',
            text: '#81a725',
        },
        text: {
            homeTitle: '#fff',
            homeSubtitle: '#fff',
            sectionTitle: '#303538',
            h3: '#81a725',
            main: '#303538',
        },
        shadow: {
            main: '#0000004d',
            light: '#00000020',
        },
        background: {
            primary: '#fffcf7',
            secondary: '#fffaf0',
            tertiary: '#fffdfb',
        },
    },

    image: {
        home: 'backgroundGreenIT.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Roboto',
            'Merriweather',
            'Lato'
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
greenIT.typography.h1 = {
    fontFamily: 'Merriweather',
    [greenIT.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [greenIT.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [greenIT.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [greenIT.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
greenIT.typography.subtitle1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    [greenIT.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [greenIT.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [greenIT.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
greenIT.typography.h2 = {
    fontFamily: 'Merriweather',
    fontWeight: 'normal',
    fontSize: '35pt',
    [greenIT.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [greenIT.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
greenIT.typography.h3 = {
    fontFamily: 'Roboto',
    fontWeight:  'bold',
    color: greenIT.palette.text.h3,
    [greenIT.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [greenIT.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [greenIT.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


greenIT.typography.body1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    color: greenIT.palette.text.main,
    [greenIT.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [greenIT.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [greenIT.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default greenIT