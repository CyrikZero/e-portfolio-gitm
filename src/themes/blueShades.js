import { createMuiTheme } from '@material-ui/core'

const blueShades = createMuiTheme({
    palette: {
        type: 'light',
        primary: {
            main: '#63808d',
        },
        secondary: {
            main: '#fff'
        },
        topBar: {
            main: '#fff',
            textOverHome: '#fff',
            text: '#4b6c7c',
        },
        footer: {
            main: '#4b6c7c',
            text: '#fafafa',
        },
        text: {
            homeTitle: '#fff',
            homeSubtitle: '#fff',
            sectionTitle: '#1c4c63',
            h3: '#1e556e',
            main: '#235870',
        },
        shadow: {
            main: '#0000004d',
            light: '#00000020',
        },
        background: {
            primary: '#e8edee',
            secondary: '#f9f9f9',
            tertiary: '#fff'
        },
    },

    image: {
        home: 'backgroundBlueShades.jpg',
        contact: {
            email: 'email-white',
            phone: 'phone-white',
            address: 'address-white'
        }
    },
    typography: {
        fontFamily: [
            'Roboto',
            'Merriweather',
            'Lato'
        ].join(','),
        h4: {
            fontFamily: 'Lato',
            fontWeight: 'bold',
            marginBottom: '20px'
        },
        body1: {
            fontSize: '13pt',
        }
    },
})


// Home title
blueShades.typography.h1 = {
    fontFamily: 'Merriweather',
    [blueShades.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [blueShades.breakpoints.only('sm')]: {
        fontSize: '30pt'
    },
    [blueShades.breakpoints.only('md')]: {
        fontSize: '30pt'
    },
    [blueShades.breakpoints.up('lg')]: {
        fontSize: '40pt',
    },
}

// Home subtitle
blueShades.typography.subtitle1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    [blueShades.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [blueShades.breakpoints.between('sm', 'lg')]: {
        fontSize: '20pt'
    },
    [blueShades.breakpoints.up('md')]: {
        fontSize: '25pt',
    },
}

// Section title
blueShades.typography.h2 = {
    fontFamily: 'Merriweather',
    fontWeight: 'normal',
    fontSize: '35pt',
    [blueShades.breakpoints.down('sm')]: {
        fontSize: '20pt'
    },
    [blueShades.breakpoints.up('md')]: {
        fontSize: '35pt'
    }
}

// Competence name
blueShades.typography.h3 = {
    fontFamily: 'Roboto',
    fontWeight:  'bold',
    color: blueShades.palette.text.h3,
    [blueShades.breakpoints.down('sm')]: {
        fontSize: '13pt'
    },
    [blueShades.breakpoints.between('sm', 'lg')]: {
        fontSize: '18pt'
    },
    [blueShades.breakpoints.up('md')]: {
        fontSize: '22pt',
    },
}


blueShades.typography.body1 = {
    fontFamily: 'Roboto',
    fontWeight: 'normal',
    color: blueShades.palette.text.main,
    [blueShades.breakpoints.down('sm')]: {
        fontSize: '8pt'
    },
    [blueShades.breakpoints.only('md')]: {
        fontSize: '12pt'
    },
    [blueShades.breakpoints.up('lg')]: {
        fontSize: '15pt',
    },
}

export default blueShades