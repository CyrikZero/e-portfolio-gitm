import config from '../static/config.json'
import SectionName from './SectionName'

class TopBar {

    static itemsList(lang) {
        const list = []
        if (config.layout.home.display) {
            list.push({
                destination: SectionName.HOME,
                label: lang.home.topBarTitle
            })
        }
        if (config.layout.aboutMe.display) {
            list.push({
                destination: SectionName.ABOUTME,
                label: lang.aboutMe.topBarTitle
            })
        }
        if (config.layout.competence.display) {
            list.push({
                destination: SectionName.COMPETENCE,
                label: lang.competence.topBarTitle
            })
        }
        if (config.layout.project.display) {
            list.push({
                destination: SectionName.PROJECT,
                label: lang.project.topBarTitle
            })
        }
        if (config.layout.hobby.display) {
            list.push({
                destination: SectionName.HOBBY,
                label: lang.hobby.topBarTitle
            })
        }
        if (config.layout.contact.display) {
            list.push({
                destination: SectionName.CONTACT,
                label: lang.contact.topBarTitle
            })
        }
        return list
    }

}

export default TopBar