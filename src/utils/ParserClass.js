
class ParserClass {

  /*  static parseSiteLayout(str, json) {
        let tab = str.split(' ')
        let map = new Map()

        // Default
        map.set('homeDisplay', false)
        map.set('aboutMeDisplay', false)
        map.set('competenceDisplay', false)
        map.set('projectDisplay', false)
        map.set('hobbyDisplay', false)
        map.set('contactDisplay', false)

        for (let i in tab) {
            switch(tab[i]) {
                case 'home':
                    map.set('homeDisplay', true)
                    map.set('homeTopBarTitle', json.home.topBarTitle)
                    break
                case 'about-me':
                    map.set('aboutMeDisplay', true)
                    map.set('aboutMeTopBarTitle', json.aboutMe.topBarTitle)
                    break
                case 'competence':
                    map.set('competenceDisplay', true)
                    map.set('competenceTopBarTitle', json.competence.topBarTitle)
                    break
                case 'project':
                    map.set('projectDisplay', true)
                    map.set('projectTopBarTitle', json.project.topBarTitle)
                    break
                case 'hobby':
                    map.set('hobbyDisplay', true)
                    map.set('hobbyTopBarTitle', json.hobby.topBarTitle)
                    break
                case 'contact':
                    map.set('contactDisplay', true)
                    map.set('contactTopBarTitle', json.contact.topBarTitle)
                    break
            }
        }

        return map
    }

    static parseHomeDisplay(str) {
        let tab = str.split(' ')
        let map = new Map()

        let imageSize = 'imageSize'
        let imageRound = 'imageRound'
        let layout = 'layout'

        // Default values
        map.set(imageRound, false)

        for (let i in tab) {
            switch(tab[i]) {
                case 'small-image':
                    map.set(imageSize, homePictureSizeEnum.SMALL)
                    break
                case 'medium-image':
                    map.set(imageSize, homePictureSizeEnum.MEDIUM)
                    break
                case 'large-image':
                    map.set(imageSize, homePictureSizeEnum.LARGE)
                    break
                case 'no-image':
                    map.set(layout, homeLayoutEnum.NOIMAGE)
                    break
                case 'left-image':
                    map.set(layout, homeLayoutEnum.LEFTIMAGE)
                    break
                case 'right-image':
                    map.set(layout, homeLayoutEnum.RIGHTIMAGE)
                    break
                case 'rond-image':
                    map.set(imageRound, true)
            }
        }

        return map
    }

    static parseContactLayout(str) {
        let tab = str.split(' ')
        let map = new Map()

        // Default
        map.set('emailDisplay', false)
        map.set('phoneDisplay', false)
        map.set('addressDisplay', false)
        map.set('facebookDisplay', false)
        map.set('instagramDisplay', false)
        map.set('githubDisplay', false)
        map.set('gitlabDisplay', false)
        map.set('linkedinDisplay', false)

        for (let i in tab) {
            switch(tab[i]) {
                case 'email':
                    map.set('emailDisplay', true)
                    break
                case 'phone':
                    map.set('phoneDisplay', true)
                    break
                case 'address':
                    map.set('addressDisplay', true)
                    break
                case 'linkedin':
                    map.set('linkedinDisplay', true)
                    break
                case 'facebook':
                    map.set('facebookDisplay', true)
                    break
                case 'instagram':
                    map.set('instagramDisplay', true)
                    break
                case 'github':
                    map.set('githubDisplay', true)
                    break
                case 'gitlab':
                    map.set('gitlabDisplay', true)
                    break
            }
        }

        return map
    }*/
}

export default ParserClass
