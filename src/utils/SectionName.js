const SectionName = {
    HOME: 'home',
    ABOUTME: 'aboutMe',
    COMPETENCE: 'competence',
    PROJECT: 'project',
    HOBBY: 'hobby',
    CONTACT: 'contact'
}

export default SectionName