import fr from '../static/languages/fr.json'
import en from '../static/languages/en.json'

const languageMap = new Map([
    ['fr', fr],
    ['en', en]
])

class Language {

    static initiateLanguage(defaultLanguage) {
        const language = localStorage.getItem('language') ||  defaultLanguage
        return languageMap.get(language)
    }

    static switchLanguage(newLanguage, setLanguage) {
        localStorage.setItem('language', newLanguage)
        setLanguage(languageMap.get(newLanguage))
    }
}

export default Language