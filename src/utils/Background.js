import SectionName from './SectionName'

class Background {

    static color(layout) {

        const colorMap = new Map()
        let counter = 0

        counter = this.checkDisplay(colorMap, layout.home.display, SectionName.HOME ,counter)
        counter = this.checkDisplay(colorMap, layout.aboutMe.display, SectionName.ABOUTME, counter)
        counter = this.checkDisplay(colorMap, layout.competence.display, SectionName.COMPETENCE, counter)
        counter = this.checkDisplay(colorMap, layout.project.display, SectionName.PROJECT, counter)
        counter = this.checkDisplay(colorMap, layout.hobby.display, SectionName.HOBBY, counter)
        this.checkDisplay(colorMap, layout.contact.display, SectionName.CONTACT, counter)
        return colorMap
    }


    static checkDisplay(map, bool, key, counter) {
        if (bool) {
            counter % 2 ?  map.set(key, 'secondary') : map.set(key, 'primary')
            counter += 1
        }
        return counter
    }


}

export default Background