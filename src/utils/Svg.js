import React from 'react'
import { ReactComponent as Competence1 } from '../svg/competence1.svg'
import { ReactComponent as Competence2 } from '../svg/competence2.svg'
import { ReactComponent as Competence3 } from '../svg/competence3.svg'
import { ReactComponent as Competence4 } from '../svg/competence4.svg'
import { ReactComponent as Competence5 } from '../svg/competence5.svg'
import { ReactComponent as Competence6 } from '../svg/competence6.svg'
import { ReactComponent as Competence7 } from '../svg/competence7.svg'
import { ReactComponent as Competence8 } from '../svg/competence8.svg'

class Svg {

    static competenceIcons(index, classes) {
        switch (index) {
            case(0):
                return <Competence1 className={classes}/>
            case(1):
                return <Competence2 className={classes}/>
            case(2):
                return <Competence3 className={classes}/>
            case(3):
                return <Competence4 className={classes}/>
            case(4):
                return <Competence5 className={classes}/>
            case(5):
                return <Competence6 className={classes}/>
            case(6):
                return <Competence7 className={classes}/>
            case(7):
                return <Competence8 className={classes}/>
        }
    }

}

export default Svg