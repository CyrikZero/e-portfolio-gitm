import React, { useState } from 'react'
import Language from './utils/Language'
import config from './static/config.json'
import MyThemeProvider from './themes/MyThemeProvider'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Background from './utils/Background'
import SectionName from './utils/SectionName'
import TopBar from './components/topbar/TopBar'
import Home from './components/home/Home'
import AboutMe from './components/aboutMe/AboutMe'
import Competence from './components/competence/Competence'
import Project from './components/project/Project'
import Hobby from './components/hobby/Hobby'
import Contact from './components/contact/Contact'
import Footer from './components/footer/Footer'

function App() {
    const [lang, setLang] = useState(Language.initiateLanguage(config.language.default))

    const colorBack = Background.color(config.layout)

    const homeContent = {
        name: lang.personalInformation.name,
        subtitle: lang.home.subtitle,
        image: lang.home.image
    }

    const aboutMeContent = {
        sectionTitle: lang.aboutMe.sectionTitle,
        image: lang.aboutMe.image,
        paragraphList: lang.aboutMe.text,
    }

    const competenceContent = {
        sectionTitle: lang.competence.sectionTitle,
        list: lang.competence.list
    }

    const projectContent = {
        sectionTitle: lang.project.sectionTitle,
        list: lang.project.list,
        learnMore: lang.settings.learnMore
    }

    const hobbyContent = {
        sectionTitle: lang.hobby.sectionTitle,
        list: lang.hobby.list,
        learnMore: lang.settings.learnMore
    }

    const contactContent = {
        sectionTitle: lang.contact.sectionTitle,
        email: lang.contact.email,
        phone: lang.contact.phone,
        address: lang.contact.address,
        cv: lang.contact.curriculumButton,
        socialMedia: lang.contact.socialMedia
    }

    return (
        <MyThemeProvider name={config.theme.color}>
            <Router>
                <TopBar
                    name={lang.personalInformation.name}
                    layout={config.layout}
                    lang={lang}
                />
                <Switch>
                    <Route path='/'>
                        {config.layout.home.display
                            ? <Home
                                version={config.layout.home.version}
                                content={homeContent}
                            />
                            : null
                        }
                        { config.layout.aboutMe.display
                            ? <AboutMe
                                version={config.layout.aboutMe.version}
                                content={aboutMeContent}
                                colorBack={colorBack.get(SectionName.ABOUTME)}
                            />
                            : null }
                        {config.layout.competence.display
                            ? <Competence
                                version={config.layout.competence.version}
                                content={competenceContent}
                                colorBack={colorBack.get(SectionName.COMPETENCE)}
                            />
                            : null
                        }
                        {config.layout.project.display
                            ? <Project
                                version={config.layout.project.version}
                                content={projectContent}
                                colorBack={colorBack.get(SectionName.PROJECT)}
                            />
                            : null
                        }
                        {config.layout.hobby.display
                            ? <Hobby
                                content={hobbyContent}
                                version={config.layout.hobby.version}
                                colorBack={colorBack.get(SectionName.HOBBY)}
                            />
                            : null
                        }
                        {config.layout.contact.display
                            ? <Contact
                                version={config.layout.contact.version}
                                socialMedia={config.layout.contact.socialMedia}
                                langShortName={lang.settings.shortLanguage}
                                content={contactContent}
                                colorBack={colorBack.get(SectionName.CONTACT)}
                            />
                            : null
                        }
                        <Footer
                            footer={lang.footer}
                            setLang={setLang}
                        />
                    </Route>
                </Switch>
            </Router>
        </MyThemeProvider>
    )
}

export default App
