import React, { useEffect, useRef } from 'react'
import { makeStyles } from '@material-ui/core'
import homeStyle from './home.style'
import Section from '../section/Section'

import LeftImage from './LeftImage'
import RightImage from './RightImage'
import NoImage from './NoImage'

const useStyles = makeStyles(homeStyle)

export default function Home(props) {
    const { version, content } = props

    const classes = useStyles()

    return (
        <Section name='home' style='home' color='fullPageImage'>
                { version === 'left-image'
                   ?  <LeftImage
                        classes={classes}
                        content={content}
                    />
                    : null
                }
                { version === 'right-image'
                    ? <RightImage
                        classes={classes}
                        content={content}
                    />
                    : null
                }
                { version === 'no-image'
                    ? <NoImage
                        classes={classes}
                        content={content}
                    />
                    : null
                }
        </Section>
    )
}