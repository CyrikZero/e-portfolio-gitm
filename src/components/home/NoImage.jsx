import React, {useEffect, useRef} from 'react'
import Typography from "@material-ui/core/Typography";
import {Power2, TweenMax} from "gsap";

export default function NoImage(props) {
    const { classes, content } = props

    let refName = useRef(null)
    let refSubtitle = useRef(null)

    useEffect(() => {
        TweenMax.staggerFrom([refName, refSubtitle], 2, {opacity: 0, x:-120, ease: Power2.easeInOut, delay: 0.4}, 0.4)
    })

    return (
        <div className={classes.textNoImage}>
            <Typography ref={elm => refName=elm} variant='h1' className={`${classes.name} ${classes.startHidden}`}>
                {content.name}
            </Typography>
            <div ref={elm => refSubtitle=elm} className={classes.startHidden} >
                {content.subtitle && content.subtitle.map((item, index) => (
                    <Typography
                        key={index}
                        variant='subtitle1'
                        className={classes.subtitle}
                    >
                        {item}
                    </Typography>
                ))}
            </div>
        </div>
    )
}