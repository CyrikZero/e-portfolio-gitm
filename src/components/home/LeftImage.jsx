import Typography from "@material-ui/core/Typography"
import React, {useEffect, useRef} from "react"
import {Power2, TweenMax} from "gsap"

export default function LeftImage(props) {
    const { classes, content} = props

    const image = process.env.PUBLIC_URL + '/images/home/' + content.image

    let refImage = useRef(null)
    let refName = useRef(null)
    let refSubtitle = useRef(null)

    useEffect(() => {
        TweenMax.staggerFrom([refImage], 2, {opacity: 0, x:-120, ease: Power2.easeInOut, delay: 0}, 0.2)
        TweenMax.staggerFrom([refName, refSubtitle], 2, {opacity: 0, x:120, ease: Power2.easeInOut, delay: 0.4}, 0.4)
    })

    return (
        <div className={classes.container}>
            <img
                ref={elm => refImage = elm}
                src={image}
                alt='profile-picture'
                className={`${classes.image} ${classes.imageLeft} ${classes.startHidden}`}
            />
            <div className={classes.text}>
                <Typography ref={elm => refName = elm} variant='h1' className={`${classes.name} ${classes.startHidden}`}>
                    {content.name}
                </Typography>
                <div ref={elm => refSubtitle = elm} className={classes.startHidden}>
                    {content.subtitle && content.subtitle.map((item, index) => (
                        <Typography
                            key={index}
                            variant='subtitle1'
                            className={classes.subtitle}
                        >
                            {item}
                        </Typography>
                    ))}
                </div>
            </div>
        </div>
    )
}
