const homeStyle = (theme) => ({

    container: {
        display: 'flex',
        height: '100%',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
            flexDirection: 'column',
        },
        [theme.breakpoints.only('md')]: {
            flexDirection: 'row',
            justifyContent:'center'
        },
        [theme.breakpoints.up('md')]: {
            flexDirection: 'row',
            padding: '0 10%',
            justifyContent: 'center'
        },
    },

    image: {
        [theme.breakpoints.down('xs')]:{
            width: '60%',
            height: 'auto',
            marginTop: '40%',
            marginBottom: '20%'
        },
        [theme.breakpoints.only('sm')]: {
            width: 'auto',
            height: '30%',
            marginTop: '30%',
            marginBottom: '10%'
        },
        [theme.breakpoints.only('md')]: {
            width: '25%',
            height: 'auto',
        },
        [theme.breakpoints.up('md')]: {
            width: '30%',
            maxWidth: '400px',
            height: 'auto',
        },
    },

    imageLeft: {
        [theme.breakpoints.only('md')]: {
            marginRight: '5%'
        },
        [theme.breakpoints.up('lg')]: {
            marginRight: '100px'
        },
    },

    imageRight: {
        [theme.breakpoints.only('md')]: {
            marginLeft: '5%'
        },
        [theme.breakpoints.up('lg')]: {
            marginLeft: '100px'
        },
    },

    text: {
        [theme.breakpoints.down('sm')]: {
            textAlign: 'center',
        },
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
    },

    name: {
        marginBottom: '20px',
        color: theme.palette.text.homeTitle,
        textShadow: '1px 1px 1px ' + theme.palette.shadow.light
    },


    subtitle: {
        marginBottom: '10px',
        color: theme.palette.text.homeSubtitle
    },

    textNoImage: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        justifyContent: 'center',
        margin: '0 10%'
    },

    startHidden: {
        opacity: '1',
    }

})

export default homeStyle
