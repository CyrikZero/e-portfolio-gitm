import React from 'react'
import Section from '../section/Section'
import MyTypography from '../myTypography/MyTypography'

import Carousel from './Carousel'
import Rectangle from '../rectangle/Rectangle'
import Gallery from '../gallery/Gallery'

export default function Project(props) {
    const { version, colorBack, content } = props

    return (
        <Section name='project' style='internSection' color={colorBack}>
            <MyTypography
                type='sectionTitle'
                label={content.sectionTitle}
            />
            { version === 'carousel'
                ? <Carousel list={content.list} />
                : null
            }
            { version === 'rectangle'
                ? <Rectangle list={content.list} learnMore={content.learnMore}/>
                : null
            }
        </Section>
    )

}
