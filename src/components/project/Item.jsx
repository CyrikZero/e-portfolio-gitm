import React from 'react'
import Paper from '@material-ui/core/Paper'
import {Typography} from "@material-ui/core"

export default function Item(props) {
    const { classes, item } = props

    const imagePath = process.env.PUBLIC_URL + '/images/project/' + item.image

    return (
        <div className={classes.itemContainer}>
            <Paper elevation={1} className={classes.paper}>
                <div className={classes.imageContainer}>
                    <img
                        src={imagePath}
                        className={classes.image}
                    />
                </div>
                <div className={classes.title}>
                    <Typography variant='h4'>
                        {item.title}
                    </Typography>
                </div>
                <div className={classes.description}>
                    <Typography variant='body1'>
                        {item.description}
                    </Typography>
                </div>
            </Paper>
        </div>
    )
}