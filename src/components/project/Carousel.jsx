import React, { useState } from 'react'
import makeStyles from '@material-ui/styles/makeStyles'
import CarouselStyle from './carousel.style'
import Laptop from './Laptop'
import Tablet from './Tablet'
import Mobile from './Mobile'

const useStyles = makeStyles(CarouselStyle)

export default function MyCarousel(props) {
    const { list } = props
    const classes = useStyles()

    return (
        <div className={classes.container}>
            <div className="carousel-wrapper">
                <Laptop
                    classes={classes}
                    list={list}
                />
                <Tablet
                    classes={classes}
                    list={list}
                />
                <Mobile
                    classes={classes}
                    list={list}
                />
            </div>
        </div>
    )
}