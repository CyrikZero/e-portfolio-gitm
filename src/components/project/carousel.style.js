const carouselStyle = (theme) => ({
    container: {
        margin: '0 10%'
    },

    laptop: {
      [theme.breakpoints.down('md')]: {
          display:'none'
      }
    },

    paperLaptop: {
        width: '25vw',
        maxWidth: '400px',
        backgroundColor: theme.palette.background.tertiary
    },

    imageContainer: {
        width: '100%'
    },

    tablet: {
      [theme.breakpoints.down('sm')]: {
          display: 'none'
      },
        [theme.breakpoints.up('lg')]: {
          display: 'none'
        }
    },

    paperTablet: {
        width: '45vw',
        maxWidth: '400px',
        backgroundColor: theme.palette.background.tertiary,
    },

    mobile: {
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    },

    paperMobile: {
        width: '60vw',
        backgroundColor: theme.palette.background.tertiary
    },

    mobileContainerList: {
      display: 'flex',
      justifyContent: 'center'
    },

    itemContainer: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        height: '99%'
    },

    image: {
        width: '100%',
        height: '100%'
    },

    title: {
        margin: '20px 0',
        textAlign: 'center',
    },

    description: {
        textAlign: 'center',
        margin: '0 20px 30px 20px'
    },

    projectButton: {
        position: 'absolute',
        backgroundColor: theme.palette.background.tertiary,
        borderRadius: '50%',
        [theme.breakpoints.up('sm')]: {
            height: '56px',
            width: '56px',
        },
        [theme.breakpoints.down('sm')]: {
            height: '30px',
            width: '30px',
        },
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        boxShadow: '1px 1px 2px ' + theme.palette.shadow.main,
        transition: 'all 0.3s linear',
        '&:hover' : {
            backgroundColor: '#00000014'
        }
    },

    ripple: {
        backgroundPosition: 'center',
        transition: 'background 0.5s',
        transitionTimingFunction: 'ease',
        '&:hover': {
            background: '#00000014 radial-gradient(circle, transparent 1%, #ffffff14 0.3%) center/15000%',
        },
        '&:active': {
            backgroundSize: '100%',
            transition: 'background 0s',
        },
      },
})

export default carouselStyle