import React, {useState} from "react";
import Fab from "@material-ui/core/Fab";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ItemsCarousel from "react-items-carousel";
import Paper from "@material-ui/core/Paper";
import {Typography} from "@material-ui/core";

export default function Laptop(props) {
    const { classes, list } = props

    const [activeItemIndex, setActiveItemIndex] = useState(0)
    const chevronWidth = 100

    const imagePath = (image) => { return process.env.PUBLIC_URL + '/images/' + image}

    return (
        <div className={classes.laptop}>
            <ItemsCarousel
                requestToChangeActive={setActiveItemIndex}
                activeItemIndex={activeItemIndex}
                numberOfCards={3}
                gutter={10}
                leftChevron={<div className={`${classes.projectButton} ${classes.ripple}`}><ChevronLeftIcon color='primary' fontSize='large'/></div>}
                rightChevron={<div className={`${classes.projectButton} ${classes.ripple}`}><ChevronRightIcon color='primary' fontSize='large'/></div>}
                outsideChevron
                chevronWidth={chevronWidth}
                infiniteLoop={true}
            >
                {list && list.map((item, index) => (
                    <div className={classes.itemContainer} key={index}>
                        <Paper elevation={1} className={classes.paperLaptop}>
                            <div className={classes.imageContainer}>
                                <img
                                    src={imagePath(item.image)}
                                    className={classes.image}
                                />
                            </div>
                            <div className={classes.title}>
                                <Typography variant='h3'>
                                    {item.title}
                                </Typography>
                            </div>
                            <div className={classes.description}>
                                <Typography variant='body1'>
                                    {item.description}
                                </Typography>
                            </div>
                        </Paper>
                    </div>
                ))}
            </ItemsCarousel>
        </div>
    )
}