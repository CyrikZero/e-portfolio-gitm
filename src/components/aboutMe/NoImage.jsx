import React from 'react'
import {Typography} from "@material-ui/core";

export default function NoImage(props) {
    const { classes, image, paragraphList } = props

    const imagePath = process.env.PUBLIC_URL + '/aboutMe/' + image

    return (
        <div className={classes.containerNoImage}>
            <div>
                {paragraphList && paragraphList.map((item, index) => (
                    <div key={index} className={classes.paragraph}>
                        <Typography variant='body1'>
                            {item}
                        </Typography>
                    </div>
                ))}
            </div>
        </div>
    )
}