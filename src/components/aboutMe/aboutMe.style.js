const aboutMeStyle = (theme) => ({

    mobileContainer: {
        display: 'flex',
        flexDirection: 'column',
        padding: '0 10%',
        alignItems: 'center',
        [theme.breakpoints.up('md')]: {
            display: 'none'
        }
    },

    mobileImageContainer: {
        display: 'flex',
        justifyContent: 'center'
    },

    mobileImage: {
        width: '100px',
        height: 'auto',
    },

    mobileParagraph: {

    },

    container: {
        display: 'flex',
        justifyContent: 'space-between',
        margin: '0 15%',
        alignItems: 'center',
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    },

    containerNoImage: {
      margin: '0 20%',
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    },

    containerCenterImage: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '0 20%'
    },

    image: {
        width: '200px',
        height: 'auto',
    },

    imageFromLeft: {
        transform: "translate(-100px, 0px)",
        opacity: 0,
    },

    imageFromRight: {
        transform: "translate(100px, 0px)",
        opacity: 0,
    },

    paragraphContainer: {
        maxWidth: '70%'
    },

    paragraphContainerCenterImage: {
        marginTop: '50px'
    },

    paragraph: {
        margin: '20px 0',
        textAlign: 'justify',
        color: theme.palette.text.main,
    },
})

export default aboutMeStyle