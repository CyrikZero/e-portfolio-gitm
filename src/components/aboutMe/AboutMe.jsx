import React, { useRef } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import AboutMeStyle from './aboutMe.style'
import Section from '../section/Section'
import MyTypography from '../myTypography/MyTypography'
import Mobile from './Mobile'
import Laptop from './Laptop'
import {useIntersection} from 'react-use'
import {Power2, TweenMax} from 'gsap'

const useStyles = makeStyles(AboutMeStyle)

export default function AboutMe(props) {
    const { version, colorBack, content } = props
    const classes = useStyles()

    const image = process.env.PUBLIC_URL + '/images/' + content.image

    let refAboutMe = useRef(null)
    let intersection = useIntersection(refAboutMe, {
        root: null,
        rootMargin: '0px',
        threshold: 0.6
    })

    const fadeIn = () => {
        TweenMax.to('.fadeAboutMe', 2, {
            opacity: 1,
            transform: 'Translate(0px, 0px)',
            ease: Power2.easeOut,
            stagger: {
                amount: 0.5
            }
        })
    }

    intersection && (intersection.isIntersecting ? fadeIn() : void(0))

    return (
        <Section name='aboutMe' style='internSection' color={colorBack}>
            <MyTypography
                type='sectionTitle'
                label={content.sectionTitle}
            />
            <div ref={refAboutMe}>
                <Mobile
                    classes={classes}
                    version={version}
                    paragraphList={content.paragraphList}
                    image={image}
                />
                <Laptop
                    classes={classes}
                    version={version}
                    paragraphList={content.paragraphList}
                    image={image}
                />
            </div>
        </Section>
    )
}