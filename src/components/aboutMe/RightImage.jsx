import React from 'react'
import {Typography} from "@material-ui/core";

export default function RightImage(props) {
    const { classes, image, paragraphList } = props

    return (
        <div className={classes.container}>
            <div className={classes.paragraphContainer}>
                {paragraphList && paragraphList.map((item, index) => (
                    <div key={index} className={classes.paragraph}>
                        <Typography variant='body1'>
                            {item}
                        </Typography>
                    </div>
                ))}
            </div>
            <div className={classes.imageContainer}>
                <img className={`${classes.image} ${classes.imageFromRight} fadeAboutMe`} src={image} />
            </div>
        </div>
    )
}