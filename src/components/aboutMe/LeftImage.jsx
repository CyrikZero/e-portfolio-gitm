import React from 'react'
import {Typography} from "@material-ui/core";

export default function LeftImage(props) {
    const { classes, image, paragraphList } = props

    return (
        <div className={classes.container}>
            <div className={classes.imageContainer}>
                <img className={`${classes.image} ${classes.imageFromLeft} fadeAboutMe`} src={image} />
            </div>
            <div className={classes.paragraphContainer}>
                {paragraphList && paragraphList.map((item, index) => (
                    <div key={index} className={classes.paragraph}>
                        <Typography variant='body1'>
                            {item}
                        </Typography>
                    </div>
                ))}
            </div>
        </div>
    )
}