import React from 'react'
import Section from '../section/Section'
import {Typography} from "@material-ui/core";

export default function Mobile(props) {
    const { version, classes, paragraphList, image } = props

    const isImage = version === 'left-image' || version === 'right-image'

    return (
        <div className={classes.mobileContainer}>
            { isImage
                ? <div className={classes.mobileImageContainer}>
                    <img className={`
                        ${classes.mobileImage} 
                        ${classes.imageFromLeft} 
                        fadeAboutMe`
                    } src={image} />
                </div>
                : null
            }
            <div>
                {paragraphList && paragraphList.map((item, index) => (
                    <div key={index} className={classes.paragraph}>
                        <Typography variant='body1'>
                            {item}
                        </Typography>
                    </div>
                ))}
            </div>
        </div>
    )
}