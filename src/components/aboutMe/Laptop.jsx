import React, { useRef } from 'react'
import LeftImage from './LeftImage'
import RightImage from './RightImage'
import NoImage from './NoImage'
import { useIntersection } from 'react-use'
import { TweenMax, Power2 } from 'gsap'

export default function Laptop(props) {
    const { classes, version, paragraphList, image } = props

    return (
        <div>
            { version === 'left-image'
                ? <LeftImage
                    classes={classes}
                    image={image}
                    paragraphList={paragraphList}
                />
                : null
            }
            { version === 'right-image'
                ? <RightImage
                    classes={classes}
                    image={image}
                    paragraphList={paragraphList}
                />
                : null
            }
            { version === 'no-image'
                ? <NoImage
                    classes={classes}
                    paragraphList={paragraphList}
                />
                : null
            }
        </div>
    )
}