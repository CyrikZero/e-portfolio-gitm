import React from 'react'
import MyTypography from '../myTypography/MyTypography'
import Section from '../section/Section'
import Rectangle from '../rectangle/Rectangle'

export default function Hobby(props) {
    const { version, colorBack, content } = props

    return (
        <Section name='hobby' style='internSection' color={colorBack}>
            <MyTypography
                type='sectionTitle'
                label={content.sectionTitle}
            />
            { version === 'rectangle'
                ? <Rectangle list={content.list} learnMore={content.learnMore}/>
                : null
            }
        </Section>
    )
}