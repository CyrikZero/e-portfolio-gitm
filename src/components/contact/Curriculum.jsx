import React from 'react'
import Button from '@material-ui/core/Button'

import cvFr from '../../static/CV-fr.pdf'
import cvEn from '../../static/CV-en.pdf'

export default function Curriculum(props) {
    const { classes, langShortName, label } = props

    const isFr = langShortName === 'fr'

    function openCV() {
        if (isFr) {
            window.open(cvFr, '_blank')
        } else {
            window.open(cvEn, '_blank')
        }
    }

    return (
        <div className={classes.buttonContainer}>
            <Button variant='contained' color='primary' onClick={ () => openCV() }>
                {label}
            </Button>
        </div>
    )
}