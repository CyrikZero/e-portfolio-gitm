import React from 'react'
import Link from '@material-ui/core/Link'

import {ReactComponent as LinkedinSvg} from '../../svg/linkedin.svg'
import {ReactComponent as GithubSvg} from '../../svg/github.svg'
import {ReactComponent as GitlabSvg} from '../../svg/git.svg'
import {ReactComponent as FacebookSvg} from '../../svg/facebook.svg'
import {ReactComponent as InstagramSvg} from '../../svg/instagram.svg'
import {ReactComponent as TwitterSvg} from '../../svg/twitter.svg'

export default function SocialMedia(props) {
    const { classes, socialMedia, content } = props

    const isLinkedin = socialMedia.linkedin.display
    const isGithub = socialMedia.github.display
    const isGitlab = socialMedia.gitlab.display
    const isFacebook = socialMedia.facebook.display
    const isInstagram = socialMedia.instagram.display
    const isTwitter = socialMedia.twitter.display

    return (
        <div className={classes.socialMediaContainer}>
            { isLinkedin
                ? <Link href={content.socialMedia.linkedin}>
                        <LinkedinSvg className={classes.socialMediaImage} />
                    </Link>
                : null
            }
            { isGithub
                ? <Link href={content.socialMedia.github}>
                    <GithubSvg className={classes.socialMediaImage} />
                </Link>
                : null
            }
            { isGitlab
                ? <Link href={content.socialMedia.gitlab}>
                    <GitlabSvg className={classes.socialMediaImage} />
                </Link>
                : null
            }
            { isFacebook
                ? <Link href={content.socialMedia.facebook}>
                    <FacebookSvg className={classes.socialMediaImage} />
                </Link>
                : null
            }
            { isInstagram
                ? <Link href={content.socialMedia.instagram}>
                    <InstagramSvg className={classes.socialMediaImage} />
                </Link>
                : null
            }
            { isTwitter
                ? <Link href={content.socialMedia.twitter}>
                    <TwitterSvg className={classes.socialMediaImage} />
                </Link>
                : null
            }
        </div>
    )
}