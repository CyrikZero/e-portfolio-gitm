import React from 'react'
import { ReactComponent as EmailSvg } from '../../svg/email.svg'
import Typography from '@material-ui/core/Typography'
import Link from '@material-ui/core/Link'

export default function Email(props) {
    const { classes, label } = props

    const email = 'mailto:' + label

    return (
        <Link href={email} className={classes.contactItemContainer}>
            <EmailSvg className={classes.icon} variant='body1' color='inherit' />
            <Typography variant='body1' className={classes.contactText}>
                {label}
            </Typography>
        </Link>
    )
}