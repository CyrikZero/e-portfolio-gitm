import React from "react";

const contactStyle = (theme) => ({

    paper: {
        [theme.breakpoints.down('xs')]: {
            margin: '0 4%',
            padding: '5%'
        },
        [theme.breakpoints.only('sm')]: {
            margin: '0 15%',
            padding: '2% 4%'
        },
        [theme.breakpoints.only('md')]: {
            margin: '0 20%',
            padding: '3% 5%'
        },
        [theme.breakpoints.up('lg')]: {
            margin: '0 30%',
            padding: '3% 5%',
        },
        backgroundColor: theme.palette.background.tertiary,
    },

    contactContainer: {
        marginBottom: '60px',
    },

    contactItemContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: '35px'
    },

    icon: {
        fill: theme.palette.primary.main,
        [theme.breakpoints.down('sm')]: {
            width: '8%',
            height: '8%',
            marginRight: '8%'
        },
        [theme.breakpoints.up('md')]: {
            width: '50px',
            height: '50px',
            marginRight: '40px'
        },
        webkitFilter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
        filter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
    },

    contactText: {
        [theme.breakpoints.down('sm')]: {
            fontSize: '12pt'
        },
        [theme.breakpoints.up('lg')]: {
            fontSize: '15pt'
        }
    },

    buttonContainer: {
        display: 'flex',
        justifyContent: 'center',
        marginBottom: '60px'

    },

    socialMediaContainer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '60px',
    },

    socialMediaImage: {
        width: '40px',
        height: '40px',
        fill: theme.palette.primary.main,
        margin: '0 8px',
        webkitFilter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
        filter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
    }

})

export default contactStyle