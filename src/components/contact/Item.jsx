import React from 'react'
import Link from '@material-ui/core/Link'
import InsideItem from './InsideItem'

export default function Item(props) {
    const { classes, type, icon, label } = props

    const email = 'mailto:' + label

    return (
        <div className={classes.item}>
            { type === 'email'
                ? <Link
                    href={email}
                    color='text'
                    underline='none'
                >
                    <InsideItem
                        classes={classes}
                        icon={icon}
                        label={label}
                    />
                    </Link>
                : <InsideItem
                    classes={classes}
                    icon={icon}
                    label={label}
                    />
            }
        </div>
    )
}