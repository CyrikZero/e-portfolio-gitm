import React from 'react'
import Typography from '@material-ui/core/Typography'

export default function InsideItem(props) {
    const { classes, icon, label} = props

    const imagePath = process.env.PUBLIC_URL + '/icons/' + icon

    return (
        <div className={classes.insideItemContainer}>
            <div className={classes.circle}>
                <img
                    className={classes.image}
                    src={imagePath} alt='logo'
                />
            </div>
            <Typography variant='body1'>
                {label}
            </Typography>
        </div>
    )
}