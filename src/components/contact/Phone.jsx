import React from 'react'
import { ReactComponent as PhoneSvg } from '../../svg/phone.svg'
import Typography from '@material-ui/core/Typography'

export default function Phone(props) {
    const { classes, label } = props


    return (
        <div className={classes.contactItemContainer}>
            <PhoneSvg className={classes.icon} variant='body1' color='textPrimary' />
            <Typography variant='body1' className={classes.contactText}>
                {label}
            </Typography>
        </div>
    )
}