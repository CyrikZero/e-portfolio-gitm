import React from 'react'
import cv from '../../static/TestDownload.pdf'
import MyTypography from '../myTypography/MyTypography'
import contactStyle from './contact.style'
import makeStyles from '@material-ui/core/styles/makeStyles'
import Section from '../section/Section'
import Paper from '@material-ui/core/Paper'
import Email from './Email'
import Phone from './Phone'
import Address from './Address'
import Curriculum from './Curriculum'
import SocialMedia from './SocialMedia'

import Link from '@material-ui/core/Link'

import Item from './Item'
import {Button} from "@material-ui/core";
import {Add} from "@material-ui/icons";

const useStyles = makeStyles(contactStyle)


export default function Contact(props) {
    const { version, colorBack, socialMedia, langShortName, content } = props
    const classes = useStyles()

    const isEmail = version === 'email-phone-address' || version === 'email-phone' || version === 'email-address' || version === 'email'
    const isPhone = version === 'email-phone-address' || version === 'email-phone' || version === 'phone-address'
    const isAddress = version === 'email-phone-address' || version === 'email-address' || version === 'phone-address'

    return(
        <Section name='contact' color={colorBack} style="internSection" >
            <MyTypography
                type='sectionTitle'
                label={content.sectionTitle}
            />
            <div className={classes.container}>
                <Paper elevation={1} className={classes.paper}>
                    <div className={classes.contactContainer}>
                        { isEmail
                            ? <Email
                                classes={classes}
                                label={content.email}
                            />
                            : null
                        }
                        { isPhone
                            ? <Phone
                                classes={classes}
                                label={content.phone}
                            />
                            : null
                        }
                        { isAddress
                            ? <Address
                                classes={classes}
                                label={content.address}
                            />
                            : null
                        }
                    </div>
                    <Curriculum
                        classes={classes}
                        langShortName={langShortName}
                        label={content.cv}
                    />
                    <SocialMedia
                        classes={classes}
                        socialMedia={socialMedia}
                        content={content}
                    />
                </Paper>
            </div>

            {/*
            <div className={classes.containerItem}>
                { displayMap.get('emailDisplay')
                    ? <Item
                        classes={classes}
                        type='email'
                        label={content.email}
                        icon='email-white.png'
                    />
                    : null
                }
                { displayMap.get('phoneDisplay')
                    ? <Item
                        classes={classes}
                        type='phone'
                        label={content.phone}
                        icon='phone-white.png'
                    />
                    : null
                }
                { displayMap.get('addressDisplay')
                    ? <Item
                        classes={classes}
                        type='address'
                        label={content.address}
                        icon='address-white.png'
                    />
                    : null
                }
            </div>
            <div className={classes.button}>
                <Button variant='contained' color='primary' }>
                    Télécharger mon cv
                </Button>
            </div>
            <div className={classes.socialMediaContainer}>
                { displayMap.get('linkedinDisplay')
                    ? <SocialMedia
                        classes={classes}
                        icon='linkedin-white.png'
                        link={content.socialMedia.linkedin}
                    />
                    : null
                }
                { displayMap.get('githubDisplay')
                    ? <SocialMedia
                        classes={classes}
                        icon='github-white.png'
                        link={content.socialMedia.github}
                    />
                    : null
                }
                { displayMap.get('gitlabDisplay')
                    ? <SocialMedia
                        classes={classes}
                        icon='gitlab-white.png'
                        link={content.socialMedia.gitlab}
                    />
                    : null
                }
                { displayMap.get('facebookDisplay')
                    ? <SocialMedia
                        classes={classes}
                        icon='facebook-white.png'
                        link={content.socialMedia.facebook}
                    />
                    : null
                }
            </div>
            */}
        </Section>
    )
}