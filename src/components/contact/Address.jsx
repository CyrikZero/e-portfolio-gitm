import React from 'react'
import { ReactComponent as HomeSvg } from '../../svg/home.svg'
import Typography from '@material-ui/core/Typography'

export default function Address(props) {
    const { classes, label } = props

    return (
        <div className={classes.contactItemContainer}>
            <HomeSvg className={classes.icon} variant='body1' color='textPrimary' />
            <Typography variant='body1' className={classes.contactText}>
                {label}
            </Typography>
        </div>
    )
}