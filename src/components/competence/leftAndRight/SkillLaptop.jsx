import React from 'react'
import Typography from '@material-ui/core/Typography'
import Svg from '../../../utils/Svg'

export default function SkillLaptop(props) {
    const { classes, item, index } = props

    return (
        <div className={`${classes[(index %2 === 0) ? 'rowLeft' : 'rowRight']}`}>
            <div className={`${classes[(index %2 === 0) ? 'skillHide' : 'skillBlank']}`}/>
            <div className={classes.skillImageText}>
                <div className={`
                    ${classes[(index % 2 === 0) ? 'imageBlank' : 'image']}
                    ${classes[(index % 2 === 0) ? 'imageBlank' : 'imageLeft']}
                    fadeCompetence
                    `}>
                    {Svg.competenceIcons(index, classes.skillImage)}
                </div>
                <div className={`${classes[(index %2 === 0) ? 'skillLeft' : 'skillRight']}`}>
                    <Typography className={classes.skillTitle} variant='h3'>
                        { item.title }
                    </Typography>
                    {item.list && item.list.map((item2, index) => (
                        <Typography variant='body1' key={index} className={classes.skillItem}>
                            {item2}
                        </Typography>
                    ))}
                </div>
                <div className={`
                    ${classes[(index %2 === 0) ? 'image' : 'imageBlank']}
                    ${classes[(index %2 === 0) ? 'imageRight' : 'imageBlank']}
                    fadeCompetence
                    `}>
                    {Svg.competenceIcons(index, classes.skillImage)}
                </div>
            </div>
            <div className={`${classes[(index %2 === 0) ? 'skillBlank' : 'skillHide']}`}/>
        </div>
    )
}