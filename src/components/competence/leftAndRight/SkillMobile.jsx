import React from 'react'
import Typography from '@material-ui/core/Typography'
import Svg from '../../../utils/Svg'

export default function SkillLaptop(props) {
    const { classes, item, index } = props

    return (
        <div className={classes.mobileContainer}>
            <div className={`${classes.mobileImage} fadeCompetence`}>
                {Svg.competenceIcons(index, classes.mobileSkillImage)}
            </div>
                <Typography className={classes.skillTitle} variant='h3'>
                    { item.title }
                </Typography>
                {item.list && item.list.map((item2, index) => (
                    <Typography variant='body1' key={index} className={classes.skillItem}>
                        {item2}
                    </Typography>
                ))}
        </div>
    )
}