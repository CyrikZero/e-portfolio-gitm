import React, { useRef } from 'react'
import SkillLaptop from './SkillLaptop'
import SkillMobile from './SkillMobile'
import { useIntersection } from 'react-use'
import { TweenMax, Power2, Power4 } from 'gsap'

export default function LeftAndRight(props) {
    const { classes, content } = props

    let refCompetence = useRef(null)
    let intersection = useIntersection(refCompetence, {
        root: null,
        rootMargin: '0px',
        threshold: 0.4
    })

    const fadeIn = () => {
        TweenMax.to('.fadeCompetence', 1.6, {
            opacity: 2,
            transform: 'Translate(0px, 0px)',
            ease: Power2.easeOut,
            stagger: {
                amount: 0.8
            }
        })
    }

    intersection && (intersection.isIntersecting ? fadeIn() : void(0))

    return(
        <div ref={refCompetence} className={classes.skillContainer}>
            <div className={classes.laptop}>
            {content.list && content.list.map((item, index) => (
                <SkillLaptop
                    key={index}
                    classes={classes}
                    item={item}
                    index={index}
                />
            ))}
            </div>
            <div className={classes.mobile}>
                { content.list && content.list.map((item, index) => (
                    <SkillMobile
                        key={index}
                        classes={classes}
                        item={item}
                        index={index}
                    />
                ))}
            </div>
        </div>
    )
}