const competenceStyle = (theme) => ({

    // SIDE PAPER : sP

    sPContainer: {
        display: 'flex',
        flexDirection: 'row',
        margin: '0 15%',
        justifyContent: 'space-between'
    },

    sPCompetenceList: {
        alignItems: 'center'
    },

    sPCompetenceItem: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: '30px',
        cursor: 'pointer'
    },

    sPCompetenceIconContainer: {
        backgroundColor: theme.palette.primary.main,
        width: '60px',
        height: '60px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: '30px',
        borderRadius: '10px 10px 10px 10px'
    },

    sPCompetenceIcon: {
        width: '50px',
        height: '50px',
        fill: theme.palette.secondary.main

    },

    sPCompetenceName: {
    },

    sPPaper: {
        width: '50%',
    },

    sPInsidePaper: {
        padding: '5% 10%',
        display: 'flex',
        flexDirection: 'column',

    },

    sPPaperTitle : {
        fontSize: '18pt',
        textTransform: 'uppercase',
        textAlign: 'center',
        marginBottom: '50px'
    },

    sPProgressionBarContainer: {
        display: 'flex',
        flexDirection: 'column',
        marginBottom: '30px'
    },

    sPProgressionBarTitle: {
        marginBottom: '5px'
    },


    laptop: {
      [theme.breakpoints.down('sm')]: {
          display: 'none'
      }
    },

    mobile: {
      [theme.breakpoints.up('md')]: {
          display: 'none'
      }
    },

    competence: {
        marginBottom: '100px'
    },

    container: {
        display: 'flex',
        justifyContent: 'space-between',
    },

    skillContainer: {
        display: 'flex',
        flexDirection: 'column',
        margin: '0 10px'
    },

    rowRight: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '50px',
    },

    rowLeft: {
        display: 'flex',
        flexDirection: 'row',
        marginBottom: '50px',
        justifyContent: 'flex-end',
    },

    skillTitle: {
      marginBottom: '10px'
    },

    skillItem: {
        marginBottom: '10px',
    },

    skillLeft: {
        flexGrow: 1,
        textAlign: 'end',
    },

    skillRight: {
        flexGrow: 1,
    },

    skillHide: {
        display: 'none'
    },

    skillBlank: {
        maxWidth: '50%',
        flexShrink: 1,
        flexGrow: 2,
        [theme.breakpoints.down('xs')]: {
            display: 'none'
        },
    },

    skillImageText: {
        display: 'flex',
        alignItems: 'center'
    },

    imageBlank: {
        display: 'none'
    },

    image: {
        backgroundColor: theme.palette.primary.main,
        width: '100px',
        height: '100px',
        marginRight: '50px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        webkitFilter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
        filter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
    },

    imageLeft: {
        marginRight: '50px',
        borderRadius: '10px 10px 10px 0px',
        opacity: 0,
        transform: "translate(-200px, 0px)",
    },

    imageRight: {
        marginLeft: '50px',
        borderRadius: '10px 10px 0px 10px',
        opacity: 0,
        transform: "translate(200px, 0px)",
    },

    skillImage: {
        width: '70px',
        height: '70px',
        webkitFilter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.main + ')',
        filter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.main + ')',
        fill: theme.palette.secondary.main
    },

    //mobile

    mobileContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '30px',
    },

    mobileImage: {
        backgroundColor: theme.palette.primary.main,
        width: '50px',
        height: '50px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '10px',
        borderRadius: '10px 10px 10px 10px',
        opacity: 0,
        transform: "translate(-200px, 0px)",
        filter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.light + ')',
    },

    mobileSkillImage: {
        width: '40px',
        height: '40px',
        webkitFilter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.main + ')',
        filter: 'drop-shadow(1px 1px 1px ' + theme.palette.shadow.main + ')',
        fill: theme.palette.secondary.main
    }
})

export default competenceStyle