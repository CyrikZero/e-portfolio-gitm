import React from 'react'
import ProgressBar from '../../progressBar/ProgressBar'
import {Typography} from "@material-ui/core";


export default function InsidePaper(props) {
    const { classes, competence } = props

    return (
        <div className={classes.sPInsidePaper}>
            <div className={classes.sPPaperTitle}>
                {competence.name}
            </div>
            { competence.list && competence.list.map((item, index) => (
                <div key={index} className={classes.sPProgressionBarContainer}>
                    <Typography variant='body1' className={classes.sPProgressionBarTitle}>
                        {item.name}
                    </Typography>
                    <ProgressBar value={item.value} />
                </div>
            ))}
            <Typography variant='body1'>
                {competence.text}
            </Typography>
        </div>
    )
}