import React, { useState } from 'react'
import Paper from '@material-ui/core/Paper'
import Svg from '../../../utils/Svg'
import Typography from '@material-ui/core/Typography'

import InsidePaper from './InsidePaper'

export default function SidePaper(props) {
    const { classes, content } = props

    const [selected, setSelected] = useState(0)

    const selectCompetence = (index) => { setSelected(index) }

    return (
        <div className={classes.sPContainer}>
            <div className={classes.sPCompetenceList}>
                { content.list && content.list.map((item, index) => (
                    <div key={index} className={classes.sPCompetenceItem} onClick={() => selectCompetence(index)}>
                        <div className={classes.sPCompetenceIconContainer}>
                            {Svg.competenceIcons(index, classes.sPCompetenceIcon)}
                        </div>
                        <Typography variant='body1' className={classes.sPCompetenceName}>
                            {item.name}
                        </Typography>
                    </div>
                ))}
            </div>
            <Paper elevation={1} className={classes.sPPaper}>
                <Typography variant='h3'>
                    { content.list && content.list.map((item, index) =>(
                        <div key={index}>
                            { index === selected
                                ? <InsidePaper
                                    classes={classes}
                                    competence={item}
                                />
                                : null
                            }
                        </div>
                    ))}
                </Typography>
            </Paper>
        </div>
    )
}