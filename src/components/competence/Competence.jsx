import React, { useRef, useState } from 'react'
import { makeStyles } from '@material-ui/core'
import MyTypography from '../myTypography/MyTypography'
import CompetenceStyle from './competence.style'
import Section from '../section/Section'
import LeftAndRight from './leftAndRight/LeftAndRight'

const useStyles = makeStyles(CompetenceStyle)

export default function Competence(props) {
    const { version, colorBack, content } = props

    const classes = useStyles()


    return (
        <Section name='competence' style='internSection' color={colorBack}>
            <MyTypography
                type='sectionTitle'
                label={content.sectionTitle}
            />
            { version === 'left-and-right'
                ? <LeftAndRight
                    classes={classes}
                    content={content}
                />
                :null
            }
        </Section>
    )
}