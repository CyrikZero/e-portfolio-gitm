import React from 'react'
import Typography from '@material-ui/core/Typography'

export default function HomeSubtitle(props) {
    const { classes, label } = props

    return (
        <div className={classes.homeSubtitle}>
            <Typography variant='h2'>
                {label}
            </Typography>
        </div>
    )
}