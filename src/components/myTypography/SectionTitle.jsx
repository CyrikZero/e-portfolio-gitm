import React from 'react'
import Typography from '@material-ui/core/Typography'

export default function SectionTitle(props) {
    const { classes, label } = props

    return (
        <div className={classes.sectionTitleContainer}>
            <div className={classes.sectionTitle}>
                <Typography variant='h2' className={classes.text}>
                    {label}
                </Typography>
                <hr className={classes.sectionTitleLine} />
            </div>
        </div>
    )
}