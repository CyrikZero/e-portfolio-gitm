import React from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import myTypographyStyle from './myTypography.style'
import HomeTitle from './HomeTitle'
import HomeSubtitle from './HomeSubtitle'
import SectionTitle from './SectionTitle'

const useStyles = makeStyles(myTypographyStyle)

export default function MyTypography(props) {
    const { type, ...others } = props
    const classes = useStyles()

    return (
        <>
            { type === 'homeTitle' ? <HomeTitle classes={classes} {...others} /> : null }
            { type === 'homeSubtitle' ? <HomeSubtitle classes={classes} {...others} /> : null }
            { type === 'sectionTitle' ? <SectionTitle classes={classes} {...others} /> : null }
            </>
    )
}