const myTypographyStyle = (theme) => ({
    homeTitle: {
        //textAlign: 'center',
        fontFamily: 'Merriweather',
        fontWeight: '500'
    },

    homeSubtitle: {
        //textAlign: 'center'
        marginTop: '20px'
    },

    sectionTitleContainer: {
        display: 'flex',
        justifyContent: 'center',
        marginBottom: '5%'
    },

    sectionTitle: {
        marginTop: '2%',
        textAlign: 'center'
    },

    text: {
        textShadow: '1px 1px 1px ' + theme.palette.shadow.light,
        color: theme.palette.text.sectionTitle,
    },

    sectionTitleLine: {
        width: '60%',
        borderColor: theme.palette.text.sectionTitle,
        boxShadow: '1px 1px 1px ' + theme.palette.shadow.light,
    }
})

export default myTypographyStyle