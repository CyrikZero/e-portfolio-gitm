import React from 'react'
import Typography from '@material-ui/core/Typography'

export default function HomeTitle(props) {
    const { classes, label } = props

    return (
        <div className={classes.homeTitle}>
            <Typography variant='h1'>
                {label}
            </Typography>
        </div>
    )
}