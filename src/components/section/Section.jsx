import React from 'react'

// Style
import makeStyles from '@material-ui/core/styles/makeStyles'
import SectionStyle from './section.style'

const useStyles = makeStyles(SectionStyle)

export default function Section(props) {
    const { style, color, name, image, children } = props

    const imagePath = process.env.PUBLIC_URL + '/images/' + image


    const classes = useStyles(imagePath)



    return (
        <div
            name={name}
            className={`${classes[`${style}`]} ${classes[`${color}`]}`}
        >
            {children}
        </div>
    )
}