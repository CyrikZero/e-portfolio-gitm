const sectionStyle = (theme) => ({
    internSection: {
        paddingTop: '50px',
        paddingBottom: '100px',
        overflow: 'hidden',
    },

    primary: {
        backgroundColor: theme.palette.background.primary
    },

    secondary: {
        backgroundColor: theme.palette.background.secondary
    },

    fullPageImage: {
        backgroundImage: 'url(' + process.env.PUBLIC_URL + '/images/home/' + theme.image.home + ')',
        height: '100%',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    }
})

export default sectionStyle