import React from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import TobBarStyle from './topbar.style'
import Mobile from "./mobile/Mobile";

import TopBarClass from '../../utils/TopBar'
import Laptop from "./laptop/Laptop";

const useStyles = makeStyles(TobBarStyle)

export default function TopBar(props) {
    const { name, lang } = props
    const classes = useStyles()

    const list = TopBarClass.itemsList(lang)

    return (
        <>
            <Mobile
                classes={classes}
                list={list}
                name={name}
            />
            <Laptop
                classes={classes}
                list={list}
                name={name}
            />
        </>
    )
}