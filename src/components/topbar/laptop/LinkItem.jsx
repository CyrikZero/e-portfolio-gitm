import { Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-scroll'

export default function LinkItem(props) {
    const { classes, destination, label, transparentNavbar } = props

    return (
        <div className={classes.linkItem}>
            <Link
                activeClass='active'
                spy={true}
                smooth={true}
                offset={-50}
                duration={500}
                to={destination}
            >
                <Typography
                    variant='body1'
                    className={classes[transparentNavbar ? 'textOverColor' : 'textOverTransparent']}
                >
                    {label}
                </Typography>
            </Link>
        </div>
    )
}