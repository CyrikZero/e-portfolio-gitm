import React, {useState} from 'react'
import LinkItem from './LinkItem'

export default function Laptop(props) {
    const { classes, name, list } = props
    const [transparentNavbar, setTransparentNavbar] = useState(false)

    const onScroll = () => {
        if (window.scrollY >= 80) {
            setTransparentNavbar(true)
        } else {
            setTransparentNavbar(false)
        }
    }

    window.addEventListener('scroll', onScroll)

    return (
        <div className={
            `${classes.topBar}
            ${classes[transparentNavbar ? 'color' : 'transparent']}`
        }>
            <div className={classes.content}>
                <div className={`${classes.left} ${classes[transparentNavbar ? 'textOverColor' : 'textOverTransparent']}`}>
                    {name}
                </div>
                <div className={classes.right}>
                    { list && list.map((item, index) => (
                        <LinkItem
                            key={index}
                            classes={classes}
                            label={item.label}
                            destination={item.destination}
                            transparentNavbar={transparentNavbar}
                        />
                    ))}
                </div>
            </div>
        </div>
    )
}