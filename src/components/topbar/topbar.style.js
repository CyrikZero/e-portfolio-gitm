const topBarStyle = (theme) => ({

   mobileContainer: {
       position: 'fixed',
       padding: '20px',
       zIndex: 2,
       [theme.breakpoints.up('md')]: {
           display: 'none'
       }
   },

    mobileName: {
        margin: '15% 10%',
        fontSize: '15pt',
        fontWeight: 'bold',
    },

    mobileList: {
        margin: '10% 10%',
        display: 'flex',
        flexDirection: 'column'
    },

    mobileItems: {
        fontSize: '13pt',
        marginBottom: '10px'
    },

    topBar: {
        position: 'fixed',
        height: '50px',
        width: '100%',
        zIndex: 2,
        transition: 'all 0.5s linear',
        opacity: 1,
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    },

    transparent: {
        backgroundColor: 'transparent',
    },

    color: {
        backgroundColor: theme.palette.topBar.main,
        boxShadow: '0px 0px 3px ' + theme.palette.shadow.main,
    },

    textOverTransparent: {
        color: theme.palette.topBar.textOverHome,
        textShadow: '1px 1px 1px ' + theme.palette.shadow.light,
    },

    textOverColor: {
        color: theme.palette.topBar.text,
        textShadow: '0px 0px 0px ' + theme.palette.shadow.light,
    },

    content: {
        height: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        margin: '0 50px',
        alignItems: 'center',
    },

    blank: {
        flexGrow: 2,
        backgroundColor: 'red'
    },

    name: {
      flexGrow: 1
    },

    text: {
        fontFamily: 'Merriweather',
        fontWeight: 'bold',
        fontSize: '15pt',
        textTransform: 'uppercase'
    },

    text2: {
        fontSize: '15pt',
        textTransform: 'uppercase'
    },

    link: {
        flexGrow: 1,
        display: 'flex',
        justifyContent: 'flex-end',
    },

    item: {
        margin: '0 5px'
    },

    left: {
        display: 'flex',
        justifyContent: 'space-between',
        fontWeight: 'bold',
        fontSize: '15pt',
        textTransform: 'uppercase'
    },

    right: {
        display: 'flex',
        justifyContent: 'flex-end',
        fontSize: '15pt',
        textTransform: 'uppercase',
        fontWeight: 'bold'
    },

    linkItem: {
        marginRight: '30px',
        cursor: 'pointer'
    }

})

export default topBarStyle