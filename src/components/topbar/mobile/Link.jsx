import { Typography } from '@material-ui/core'
import React from 'react'
import { Link } from 'react-scroll'

export default function LinkItem(props) {
    const { destination, label, handleClose } = props

    return (
        <div>
            <Link
                activeClass='active'
                spy={true}
                smooth={true}
                offset={-50}
                duration={500}
                to={destination}
            >
                <Typography variant='body1'>
                    {label}
                </Typography>
            </Link>
        </div>
    )
}