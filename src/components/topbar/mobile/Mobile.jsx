import React, { useState } from 'react'
import MenuIcon from '@material-ui/icons/Dehaze'
import Fab from '@material-ui/core/Fab'
import Drawer from '@material-ui/core/Drawer'
import Link from './Link'

export default function Mobile(props) {
    const { classes, name, list } = props
    const [isOpen, setIsOpen] = useState(false)

    const handleOpen = () => { setIsOpen(true) }
    const handleClose = () => { setIsOpen(false) }

    return (
        <div className={classes.mobileContainer}>
            <Fab color='primary' onClick={handleOpen}>
                <MenuIcon color='secondary'/>
            </Fab>
            <Drawer anchor='left' open={isOpen} onClose={handleClose}>
                <div className={classes.mobileName}>
                    {name}
                </div>
                <div className={classes.mobileList}>
                    { list && list.map((item, index) => (
                        <div key={index} className={classes.mobileItems}>
                            <Link
                                label={item.label}
                                destination={item.destination}
                            />
                        </div>
                    ))}
                </div>
            </Drawer>
        </div>
    )
}