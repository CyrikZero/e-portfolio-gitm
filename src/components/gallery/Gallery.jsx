import React from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import GalleryStyle from './gallery.style'
import Item from './Item'
import GridList from '@material-ui/core/GridList'
const useStyles = makeStyles(GalleryStyle)

export default function Gallery(props) {
    const { list } = props
    const classes = useStyles()

    return (
        <div className={classes.container}>
            <GridList className={classes.gridList} cellHeight={50} cols={2} spacing={0}>
                {list && list.map((item, index) => (
                    <Item
                        key={index}
                        classes={classes}
                        item={item}
                    />
                ))}
            </GridList>
        </div>
    )
}