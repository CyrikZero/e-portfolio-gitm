const galleryStyle = (theme) => ({

    container: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
        overflow: 'hidden',
        //backgroundColor: theme.palette.background.tertiary,
        width: '40%'
    },

    imageContainer: {
        width: '50%',
        height: '50%'
    },

    image: {
       width: 'fit-content',
       height: 'auto'
    }

})

export default galleryStyle
