import React from 'react'
import GridListTile from '@material-ui/core/GridListTile'

export default function Item(props) {
    const { classes, item } = props

    const imagePath = process.env.PUBLIC_URL + '/images/' + item.image

    return (
        <GridListTile className={classes.imageContainer}>
            <img className={classes.image} src={imagePath} alt={item.title} />
        </GridListTile>
    )
}