const rectangleStyle = (theme) => ({
    hobby: {
        backgroundColor: 'RGB(70,70,70,0.03)'
    },

    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        margin: '0 5%',
        flexWrap: 'wrap'
    },

    paper: {
        marginBottom: '50px',
        display: 'flex',
        maxWidth: '800px',
        width: '60%',
        height: '250px',
        backgroundColor: theme.palette.background.tertiary,
        [theme.breakpoints.only('md')]: {
            height: '150px',
            width: '70%'
        },
        [theme.breakpoints.only('sm')]: {
            height: '100px',
            width: '80%'
        },
        [theme.breakpoints.only('xs')]: {
            height: '100px',
            width: '90%'
        }
    },

    imageRight: {
        flexDirection: 'row'
    },

    imageLeft: {
        flexDirection: 'row-reverse'
    },

    image: {
        height: '100%',
    },

    text: {
        display: 'flex',
        flexDirection: 'column',
        margin: '10px 50px',
        [theme.breakpoints.only('md')]: {
            margin: '10px 40px'
        },
        [theme.breakpoints.only('sm')]: {
            margin: '10px 30px'
        },
        [theme.breakpoints.only('xs')]: {
            margin: '10px 20px'
        }
    },

    title: {
        margin: '10px 0'
    },

    description: {
        marginBottom: '10px'
    },

    link: {
        display: 'flex',
        alignItems: 'center'
    }
})

export default rectangleStyle