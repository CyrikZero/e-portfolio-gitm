import React from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import RectangleStyle from './rectangle.style'

import Item from './Item'
import LinkItem from './LinkItem'

const useStyles = makeStyles(RectangleStyle)

export default function Rectangle(props) {
    const { list, learnMore } = props

    const classes = useStyles()

    return (
        <div className={classes.container}>
            { list && list.map((item, index) => (
                <React.Fragment key={index}>
                    {item.link.display
                        ? <Item
                            classes={classes}
                            item={item}
                            index={index}
                        />
                        : <Item
                            item={item}
                            classes={classes}
                            index={index}
                        />
                    }
                </React.Fragment>
            ))}
        </div>
    )
}