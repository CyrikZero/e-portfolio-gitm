import React from 'react'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Link from '@material-ui/core/Link'
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

export default function Item(props) {
    const { item, classes, index, learnMore } = props

    const imagePath = process.env.PUBLIC_URL + '/images/' + item.image

    return (
        <Paper
            elevation={1}
            className={`${classes.paper} ${classes[index %2 === 0 ? 'imageRight' : 'imageLeft']}`}
        >
                <img
                    alt='image'
                    className={classes.image}
                    src={imagePath}
                />
            <div className={classes.text}>
                <Typography variant='h3' className={classes.title}>
                    {item.title}
                </Typography>
                <Typography  className={classes.description} variant='body1'>
                    {item.description}
                </Typography>
                <Typography className={classes.link} variant='body2'>
                    <Link href={item.link.url}>
                        { learnMore }
                        {' >'}
                    </Link>
                </Typography>
            </div>
        </Paper>
    )
}