import React, { useState } from 'react'
import makeStyles from '@material-ui/core/styles/makeStyles'
import FooterStyle from './footer.style'
import Link from '@material-ui/core/Link'
import TranslateIcon from '@material-ui/icons/Translate'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Language from '../../utils/Language'

const useStyles = makeStyles(FooterStyle)

export default function Footer(props) {
    const { footer, setLang } = props
    const classes = useStyles()

    const [isOpen, setIsOpen] = useState(false)
    const [anchorEl, setAnchorEl] = React.useState(null)
    const handleOpen = (event) => { setAnchorEl(event.currentTarget) }
    const handleClose = () => { setAnchorEl(null) }
    const changeLanguage = (newLang) => { Language.switchLanguage(newLang, setLang); handleClose(false) }

    return(
        <div className={classes.container}>
            <div className={classes.language}>
                <Button onClick={handleOpen} startIcon={<TranslateIcon />} className={classes.button}>
                    Langue
                </Button>
                <Menu
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                    keepMounted
                    anchorEl={anchorEl}
                >
                    <MenuItem  onClick={() => changeLanguage('fr')}>
                        Français
                    </MenuItem>
                    <MenuItem onClick={() => changeLanguage('en')}>
                        English
                    </MenuItem>
                </Menu>
            </div>
            <div className={classes.credits}>
                GITM 2021
            </div>
            <div className={classes.thanks}>
                <div>{footer.thanks}<Link className={classes.link} color='text' href='https://www.flaticon.com'>flaticon.com</Link></div>
            </div>
        </div>
    )
}