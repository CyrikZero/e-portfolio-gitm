const footerStyle = (theme) => ({
    container: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: theme.palette.footer.main,
        height: '80px',
        padding: '0 10%',
        textAlign: 'center',
        color: theme.palette.footer.text,
    },

    language: {
        width: '20%'
    },

    button: {
        color: theme.palette.footer.text,
    },

    credits: {
        width: '20%',
        textAlign: 'center'
    },

    thanks: {
        width: '20%',
        fontSize: '10pt'
    },

    link: {
        cursor: 'pointer'
    }

})

export default footerStyle