import fr from './static/languages/fr.json'
import en from './static/languages/en.json'

const languageMap = new Map([
    ['fr', fr],
    ['en', en]
])

export default languageMap