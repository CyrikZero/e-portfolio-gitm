# ePortfolio

AJOUTER INTRO

## Créer votre site personalisé !

### Le déploiment

#### Heroku


#### OVH


#### Local


### Ajouter vos informations

Afin de personaliser votre site, vous aurez trois fichiers à modifier :
- `src/static/config.json`
- `src/static/languages/fr.json`
- `src/static/languages/en.json`

Le premier est un fichier de configuration vous permettant de choisir le thème mais également les dispositions des composants.
Vous trouverez dedans :
- un espace pour gérer le langage par défaut de votre site. Inscrivez "fr" ou "en"
```
"language": {
    "default": "fr"
}
```
- un espace pour gérer le thème
```
"theme": {
    "color": "name-of-the-theme"
}
```
- La partie layout vous permet de choisir la configuration de chaque composant. Si vous souhaitez ne pas afficher un composant, il suffit de passer son display a false. La version vous permet de choisir le layout que vous voulez utiliser (lorsque plusieurs sont disponible)
```
"layout": {
    "home": {
        "display": true,
        "version": "name-of-the-version"
    }
}
```

Les deux fichiers qui suivent sont le texte qui s'affichera, en français et en anglais.

#### Choisi ton thème ! 

Plusieurs thèmes sont disponibles
- classic
- unicorn
- businessSuit

##### Unicorn
![](images/unicorn.png)

#### Accueil

Le composant Home propose plusieurs versions :
- left-image
- right-image
- no-image

```
"personalInformation": {
    "name": "Amandine Delamare" // Ceci sera le titre
  },
  "home": {
    "topBarTitle": "Accueil", // Ceci sera le nom afficher dans la top bar
    "subtitle": [ // Ceci est le sous-titre de l'accueil, chaque string sera interpréter comme un paragraphe
      "Ingénieure en informatique",
      "Chef de projet chez Value Médical"
      ],
    "image": "profile.png" // Ceci est le nom de votre image, que devez ajouter dans '/public/images/home/'
  },
```
#### A propos de moi

Le composant à propos propose plusieurs versions :
- left-image
- right-image
- no-image

```
"aboutMe": {
    "topBarTitle": "A propos", // Ceci sera le nom afficher dans la top bar
    "sectionTitle": "A propos de moi", // Ceci sera le nom de la section
    "image":"crown.png", // Ceci est le nom de votre image, que devez ajouter dans '/public/images/aboutMe/'
    "text": [ // Ceci est le texte, chaque string sera interpréter comme un paragraphe
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pretium dictum ligula ut cursus. Praesent feugiat nulla vel feugiat ornare. Nullam at aliquam neque, congue posuere mi. Mauris luctus neque ac ante consequat luctus. Duis vel varius ex. Praesent id tristique ex, ut dignissim neque. Donec viverra rhoncus est, eget sollicitudin dolor fringilla sit amet. ",
      "Nullam aliquam aliquet efficitur. Nullam vitae magna varius, iaculis eros vitae, posuere odio. Curabitur nec ultrices quam, non dapibus nulla. Pellentesque ultrices ante molestie, placerat nunc ut, vulputate lorem. Nunc id lacus iaculis, interdum sapien at, dictum sem. Morbi placerat tincidunt magna nec finibus. Etiam scelerisque luctus neque. Praesent mattis eleifend lacus quis luctus."
    ]
  },
```

#### Competences

Le composant competence propose plusieurs versions :
- left-and-right

Afin d'afficher une image pour chaque competence, il vous faut aller dans le dossier `src/svg/` et choisir les icones que vous souhaitez utiliser pour chaque compétence. Renommer la `competence1.svg` en changeant le numéro en fonction de la position de la compétence dans la liste.

```
"competence" : {
    "topBarTitle": "Compétences", // Ceci sera le nom afficher dans la top bar
    "sectionTitle": "Mes compétences", // Ceci sera le nom de la section
    "list": [ // Ceci est la liste de vos competences
      {
        "name": "Gestion de projet", // Ceci est le nom de votre première compétence
        "list": [ // Ceci est la liste des IDD au sein de la competence
          "Réaliser une analyse systémique", 
          "Faire la matrice probabilité/impact des risques",
          "Analyser une entreprise à travers son dépôt de code",
          "Mener un entretien semi-directif"
        ],
        "image": "competence1.svg"
      },
    ]
}
```

#### Projet

Le composant projet propose plusieurs versions :
- carousel

```
"project": {
    "topBarTitle": "Projets", // Ceci sera le nom afficher dans la top bar
    "sectionTitle": "Mes projets", // Ceci sera le nom de la section
    "list": [ // Ceci est la liste de vos projets 
      {
        "image": "img1.png", // Ceci est le nom de votre image, que devez ajouter dans '/public/images/project/'
        "title": "Projet 1",
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pretium dictum ligula ut cursus."
      },
    ]
}
```

#### Loisirs

Le composant loisir propose plusieurs versions :
- rectangle

```
"hobby": {
    "topBarTitle": "Loisirs", // Ceci sera le nom afficher dans la top bar
    "sectionTitle": "Mes loisirs", // Ceci sera le nom de la section
    "list": [ // Ceci est la liste de vos projets 
      {
        "name": "Equitation", // Ceci est le nom de votre image, que devez ajouter dans '/public/images/project/'
        "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc pretium dictum ligula ut cursus."
        "image": "equitation.jpeg" // Ceci est le nom de votre image, que vous devez ajouter dans `/public/images/hobby/`    
      },
    ]
}
```

####

